class IndigoError(Exception):
    pass


class DSLQueryError(IndigoError):
    pass
