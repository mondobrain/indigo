import json

import numpy as np
import pandas as pd
import spacy

from indigo.nlp.pipeline import KnownEntityRuler


def ingest(dataset):
    """
    function to receive CSV and return a
    Pandas DataFrame for ease of additional data manipulation
    """
    if isinstance(dataset, pd.DataFrame):
        frame = dataset
    else:
        frame = pd.read_csv(dataset)

    return frame


class Tent(object):
    """
    The Tent object ephemerally stores information about a dataset and also infers any
    entities that are associated with a natural language query as related to the dataset.

    A Tent object is generally pitched with a dataset and/or a natural language query.
    Once pitched, a serialized set of stakes can be accessed, representing key meta-data
    from the data set as well as entities that were extracted from the query.

    Tents simplify use of the custom Spacy pipeline object KnownEntityRuler.

    Key entities are:
    - COL: column entities
    - CLASS: modalities of the columns

    Additional entities are:
    - OUTCOME: outcome variable related to Mondobrain Solve
    - CONST: constraint that defines a preconditional filter equality
    - OPER: the Mondobrain Solve operation to perform

    **Parameters**

    dataset: Pandas DataFrame-like or buffered CSV object
        The dataset used to pitch the Tent

    question: Str
        Text object from which to extract any entities and stored in order
        to structure a MondoBrain solve query or other supported queries.

    **Examples**

    >>> import pandas as pd
    >>> from indigo.camp import Tent
    ...
    >>> dataframe = pd.read_csv("path/to/some/file")
    >>> tent = Tent(dataset=dataframe)
    >>> tent.stakes
    {'features': [{'key': 'Level of Risk',
    'type': 'discrete',
    'modalities': ('1. Good', '2. Average', '3. Risk'),
    'counts': (2019, 439, 88)}},
    ...
    >>> tent.pitch(question= \"""What is the significance of level of risk for the class "3. Risk" using
                Date,
                previous home ownership = rent,
                salary > 1000, and
                employment length greater than or equal to 1000?\""")
    >>> tent.stakes['entities']
    {'explorable': ['Date', 'Previous Home Ownership', 'employment length'],
     'outcome': 'Salary',
     'operation': {'type': 'continuous', 'value': 'max'},
     'constraints': [{'feature': 'Previous Home Ownership',
       'constraint': '=',
       'class': 'rent'}]}

    **Methods**

    """  # NOQA E501

    def __init__(self, stakes=None, dataset=None, question=None, pipeline=None):

        self.frame = None
        self.stakes = stakes
        self._nlp = pipeline if pipeline is not None else spacy.load("en_core_web_md")
        self.pitch(stakes, dataset, question)

    def pitch(self, stakes=None, dataset=None, question=None):
        """
        Set up a Tent with either a dataset or question or both.

        This extracts all features as well as entities to form relevant meta-data
        related to the dataset and/or natural language query.
        """

        self.question = question
        if stakes is not None:
            self.stakes = stakes
        if dataset is not None:
            self.frame = ingest(dataset)

        self.set_stakes()

    def get_info(self):
        info = dict()
        for col, dtype in self.frame.dtypes.items():
            if dtype == np.object:
                modalities, counts = list(
                    zip(*pd.value_counts(self.frame[col]).items())
                )
                info[col] = {"modalities": modalities, "counts": counts}
            else:
                info[col] = self.frame[col].describe().to_dict()
                info[col]["range"] = tuple((info[col]["min"], info[col]["max"]))
        return info

    def get_entities(self):

        if not self._nlp.has_pipe("stakes"):
            self._nlp.add_pipe(KnownEntityRuler(self._nlp, self.stakes), name="stakes")

        operation_dict = {
            "increase": "max",
            "decrease": "min",
            "minimize": "min",
            "maximize": "max",
        }

        doc = self._nlp(self.question)

        entities = dict()
        entities["explorable"] = [t.text for t in doc._.explorable]
        entities["outcome"] = doc._.outcome.text
        if doc._.outcome_class:
            entities["operation"] = {
                "type": "discrete",
                "value": doc._.outcome_class.text,
            }
        elif doc._.operation.text.lower() in operation_dict:
            entities["operation"] = {
                "type": "continuous",
                "value": operation_dict[doc._.operation.text.lower()],
            }
        else:
            entities["operation"] = {"type": "discrete", "value": doc._.operation.text}
        entities["constraints"] = doc._.constraints

        return entities

    def set_stakes(self):
        """
        Set all meta-data related to the dataset and/or natural language query.
        """
        if self.stakes is None:
            self.stakes = dict()
        if self.frame is not None:
            info = self.get_info()
            feature_list = list()
            for col in self.frame.columns:
                feature_dict = dict()
                feature_dict["key"] = col
                dtype = self.frame.dtypes[col]
                if dtype == np.object:
                    feature_dict["type"] = "discrete"
                elif np.issubdtype(dtype, np.datetime64):
                    feature_dict["type"] = "timeseries"
                else:
                    feature_dict["type"] = "continuous"
                for key, val in info[col].items():
                    feature_dict[key] = val
                feature_list.append(feature_dict)

            self.stakes["features"] = feature_list
            self.stakes["shape"] = self.frame.shape

        if self.question is not None:
            self.stakes["entities"] = self.get_entities()

    def pack(self, format="json"):
        """
        Pack up the Tent in the form the form of a serialized object.
        """
        if format == "json":
            package = json.dumps(self.stakes)

        return package
