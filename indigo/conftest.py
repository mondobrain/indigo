import os

import pandas as pd
import pytest


@pytest.fixture
def datapath():
    """
    Get the path to a data file.
    Parameters
    ----------
    path : str
        Path to the file, relative to ``indigo/tests/``
    Returns
    -------
    path including ``indigo/tests``.
    Raises
    ------
    ValueError
        If the path doesn't exist
    """
    BASE_PATH = os.path.join(os.path.dirname(__file__), "tests")

    def deco(*args):
        path = os.path.join(BASE_PATH, *args)
        if not os.path.exists(path):
            raise ValueError(f"Could not find file {path}")
        return path

    return deco


@pytest.fixture
def bank_risk(datapath):
    """
    The bank risk dataset.
    """
    return pd.read_csv(datapath("data", "bank_risk.csv"))
