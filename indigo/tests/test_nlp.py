import pytest
import spacy

from indigo.camp import Tent
from indigo.nlp.pipeline import KnownEntityRuler


@pytest.fixture
def serialized(bank_risk):
    return Tent(dataset=bank_risk).stakes


def test_pipeline(serialized, subtests):
    nlp = spacy.load("en_core_web_md")

    pipe = KnownEntityRuler(nlp, serialized)
    nlp.add_pipe(pipe)

    with subtests.test(msg="meta"):
        pipe = nlp.get_pipe("indigo_knownentityruler")

        assert pipe.meta == serialized

    with subtests.test(msg="columns"):
        message = """
        What is the significance of level of risk for the class "3. Risk" using
                Date,
                previous home ownership = rent,
                salary > 1000, and
                employment length greater than or equal to 1000?
        """
        doc = nlp(message)

        columns = [tok.text.lower() for tok in doc if tok._.is_col]
        features = [feat["key"].lower() for feat in serialized["features"]]
        assert all(col in features for col in columns)

    with subtests.test(msg="modalities"):
        modalities = [tok.text.lower() for tok in doc if tok._.is_class]
        classes = ["3. Risk", "rent"]
        assert (
            all(cls.lower() in modalities for cls in classes) and len(modalities) == 2
        )

    with subtests.test(msg="outcome"):
        outcome = doc._.outcome.text.lower()
        assert outcome == "level of risk"

    with subtests.test(msg="outcome_class"):
        outcome_class = doc._.outcome_class.text.lower()
        assert outcome_class == "3. risk"

    with subtests.test(msg="operation"):
        operation = doc._.operation.text.lower()
        assert operation == "what"
