from typing import Dict

import numpy as np
import spacy
from spacy.matcher import Matcher, PhraseMatcher
from spacy.tokens import Doc, Span, Token

from indigo.error import DSLQueryError


class KnownEntityRuler(object):
    """
    NLP KnownEntityRuler object.

    Custom pipeline object that sets entity annotations based on a provided serialized dataset
    metadata. Extracted entities include columns and modalities of those column variables.

    Additionally if a text query is provided, other relevant entities are set on the
    object in order to allow easy composition of Mondobrain Solves as well as potentially
    enable other useful applications.

    Key entities are:
    - COL: column entities
    - CLASS: modalities of the columns

    Additional entities are:
    - OUTCOME: outcome variable related to Mondobrain Solve
    - CONST: constraint that defines a preconditional filter equality
    - OPER: the Mondobrain Solve operation to perform

    **Parameters**

    nlp: spacy.language
        The initial Spacy pipeline language component

    DatasetMetaData: Dict
        Dictionary object composed of the following:
        - features: dataset columns
            - key: column labels
            - type: feature data type (discrete or continuous)
            - modalities: unique classes of the discrete column
            - min: lower bound of the continuous column
            - max: upper bound of the continuous column

    **Examples**

    >>> import spacy
    >>> import json
    >>> from indigo.nlp.pipeline import KnownEntityRuler
    ...
    >>> meta_data = json.loads("path/to/some/file")
    >>> pipeline = spacy.load("en_core_web_md")
    >>> pipeline.add_pipe(KnownEntityRuler(pipeline, meta_data))
    >>> pipe = pipeline.get_pipe('indigo_knownentityruler')
    >>> pipe.meta
    {'features': [{'key': 'Level of Risk',
    'type': 'discrete',
    'modalities': ('1. Good', '2. Average', '3. Risk'),
    'counts': (2019, 439, 88)},
    ...
    >>> doc = pipeline(\"""What is the significance of level of risk for the class "3. Risk" using
                Date,
                previous home ownership = rent,
                salary > 1000, and
                employment length greater than or equal to 1000?\""")

    **Methods**

    """  # NOQA E501

    name = "indigo_knownentityruler"

    def __init__(self, nlp: spacy.language, DatasetMetaData: Dict):
        """Initialise the pipeline component. The shared nlp instance is used
        to initialise the phrase_matcher with the shared vocab, get the label ID and
        generate Doc objects as phrase match patterns.
        """
        self.meta = DatasetMetaData

        # Set up the PhraseMatcher - it can now take Doc objects as patterns,
        # so even if the list of columns/classes is long, it's very efficient
        self.phrase_matcher = PhraseMatcher(nlp.vocab, attr="LOWER")
        self.matcher = Matcher(nlp.vocab)

        # Setup columns as Doc objects & add
        column_patterns = [nlp(feat["key"]) for feat in self.meta["features"]]
        self.phrase_matcher.add("COL", column_patterns)

        # Setup operations as Doc objects & add
        operation_patterns = [
            nlp(oper)
            for oper in [
                "increase",
                "decrease",
                "explain",
                "minimize",
                "maximize",
                "why",
                "how",
                "what",
            ]
        ]
        self.phrase_matcher.add("OPER", operation_patterns)

        # Setup classes as Doc object & add
        discrete_classes = np.concatenate(
            (
                [
                    feat["modalities"]
                    for feat in self.meta["features"]
                    if feat["type"] == "discrete"
                ]
            )
        )
        class_patterns = [nlp(str(classes)) for classes in discrete_classes]
        self.phrase_matcher.add("CLASS", class_patterns)

        # Setup constraints as Doc objects & add
        constraint_patterns = [
            [{"TEXT": ">"}, {"LIKE_NUM": True}],
            [{"TEXT": "greater"}, {"TEXT": "than"}, {"LIKE_NUM": True}],
            [{"TEXT": ">"}, {"TEXT": "="}, {"LIKE_NUM": True}],
            [
                {"TEXT": "greater"},
                {"TEXT": "than"},
                {"TEXT": "or"},
                {"TEXT": "equal"},
                {"TEXT": "to"},
                {"LIKE_NUM": True},
            ],
            [{"TEXT": "<"}, {"LIKE_NUM": True}],
            [{"TEXT": "less than"}, {"LIKE_NUM": True}],
            [{"TEXT": "<"}, {"TEXT": "="}, {"LIKE_NUM": True}],
            [{"TEXT": "less than or equal to"}, {"LIKE_NUM": True}],
            [
                {"TEXT": "between"},
                {"LIKE_NUM": True},
                {"LOWER": "and"},
                {"LIKE_NUM": True},
            ],
            [{"TEXT": {"IN": ["="]}}],
        ]

        self.matcher.add("CONST", None, *constraint_patterns)

        # Register attribute on the Token. We'll be overwriting this based on
        # the matches, so we're only setting a default value, not a getter.
        Token.set_extension("is_col", default=False, force=True)
        Token.set_extension("is_oper", default=False, force=True)
        Token.set_extension("is_class", default=False, force=True)
        Token.set_extension("is_const", default=False, force=True)

        # Register attributes on Doc and Span via a getter that checks if one of
        # the contained tokens is set to is_col == True.
        Doc.set_extension("has_col", getter=self.has_col, force=True)
        Span.set_extension("has_col", getter=self.has_col, force=True)
        Doc.set_extension("has_class", getter=self.has_class, force=True)
        Span.set_extension("has_class", getter=self.has_class, force=True)
        Doc.set_extension("has_const", getter=self.has_class, force=True)
        Span.set_extension("has_const", getter=self.has_class, force=True)

        # Register attributes to get operation, outcome, & explorable
        Doc.set_extension("operation", getter=self.get_operation, force=True)
        Doc.set_extension("outcome", getter=self.get_outcome, force=True)
        Doc.set_extension("explorable", getter=self.get_explorable, force=True)
        Doc.set_extension("outcome_class", getter=self.get_outcome_class, force=True)
        Doc.set_extension("constraints", getter=self.get_constraints, force=True)

    def __call__(self, doc: Doc):
        """Apply the pipeline component on a Doc object and modify it if matches
        are found. Return the Doc, so it can be processed by the next component
        in the pipeline, if available.
        """
        matches = self.phrase_matcher(doc) + self.matcher(doc)
        self._explorable_constraints = []
        spans = []
        doc.ents = ()
        for match_id, start, end in matches:
            # Generate Span representing the entity & set label
            entity = Span(doc, start, end, label=match_id)
            spans.append(entity)

            # Set custom attribute on each token of the entity
            for token in entity:
                if entity.label_ == "COL":
                    token._.set("is_col", True)

                if entity.label_ == "CLASS":
                    token._.set("is_class", True)

                if entity.label_ == "OPER":
                    token._.set("is_oper", True)

                if entity.label_ == "CONST":
                    token._.set("is_const", True)

            # Overwrite doc.ents and add entity – be careful not to replace!
            doc.ents = list(doc.ents) + [entity]

        for span in spans:
            # Iterate over all spans and merge them into one token. This is done
            # after setting the entities – otherwise, it would cause mismatched
            # indices!
            span.merge()

        return doc  # return doc to allow continuation of pipeline

    def has_col(self, tokens):
        """Getter for Doc and Span attributes. Returns True if one of the tokens
        is a column. Since the getter is only called when we access the
        attribute, we can refer to the Token's 'is_col' attribute here,
        which is already set in the processing step.
        """
        return any(t._.get("is_col") for t in tokens)

    def has_class(self, tokens):
        return any(t._.get("is_class") for t in tokens)

    def get_operation(self, tokens):
        try:
            # should probably use `ent_type` (uint64)
            operations = [token for token in tokens if token._.is_oper]
            operation = operations[0]
        except IndexError:
            raise DSLQueryError(
                "Unable to find an operation. Try a different text string"
            )

        return operation

    def get_outcome(self, tokens):
        try:
            columns = [token for token in tokens if token._.is_col]
            return columns[0]
        except IndexError:
            raise DSLQueryError("Unable to find the column to optimize")

    def _columns(self, tokens):
        return [token for token in tokens if token._.is_col]

    def _classes(self, tokens):
        return [token for token in tokens if token._.is_class]

    def _constraints(self, tokens):
        return [token for token in tokens if token._.is_const]

    def get_explorable(self, tokens):
        columns = self._columns(tokens)
        outcome = self.get_outcome(tokens)
        return [col for col in columns if col != outcome]

    def get_constraints(self, doc):
        def get_constraint_type(index, entities):
            before, current = entities[index - 1], entities[index]
            if index + 1 != len(entities):
                after = entities[index + 1]
            else:
                after = entities[index]
            constraint_pattern = None
            if (before.label_, current.label_) == ("COL", "CONST"):
                constraint_pattern = "continuous"
            if (before.label_, current.label_, after.label_) == (
                "COL",
                "CONST",
                "CLASS",
            ):
                constraint_pattern = "discrete"
            return constraint_pattern

        def get_constraint_dict(index, entities):
            before, current = entities[index - 1], entities[index]
            if index + 1 != len(entities):
                after = entities[index + 1]
            else:
                after = entities[index]
            if (before.label_, current.label_) == ("COL", "CONST"):
                constraint_dict = {"feature": before.text, "constraint": current.text}
            if (before.label_, current.label_, after.label_) == (
                "COL",
                "CONST",
                "CLASS",
            ):
                constraint_dict = {
                    "feature": before.text,
                    "constraint": current.text,
                    "class": after.text,
                }
            return constraint_dict

        constraints_list = list()
        for idx, entity in enumerate(doc.ents):
            if entity.start == 0:
                continue
            else:
                if get_constraint_type(idx, doc.ents) == "continuous":
                    constraint_dict = get_constraint_dict(idx, doc.ents)
                elif get_constraint_type(idx, doc.ents) == "discrete":
                    constraint_dict = get_constraint_dict(idx, doc.ents)
                else:
                    constraint_dict = None
                if constraint_dict:
                    constraints_list.append(constraint_dict)

        return constraints_list

    def get_outcome_class(self, doc):
        classes = self._classes(doc)
        discrete_class = None
        if classes:
            discrete_class = classes[0]
            for idx, entity in enumerate(doc.ents):
                if entity.text == discrete_class.text:
                    before = doc.ents[idx - 1]
                    if before.text != doc._.outcome.text:
                        discrete_class = None
                        break

        return discrete_class
