from pandas.core.series import Series


class Column(object):
    def __init__(self, series: Series):
        self.series = series

    @property
    def name(self):
        return self.series.name

    @property
    def targets(self):
        if self.series.dtype in [
            "int16",
            "int32",
            "int64",
            "float16",
            "float32",
            "float64",
        ]:
            return ["minimize", "maximize"]

        return self.series.unique()
