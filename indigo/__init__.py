# flake8: noqa
import logging

from indigo.nlp.pipeline import KnownEntityRuler

__author__ = "MondoBrain"
__version__ = "0.0.0"


# Set up logging to ``/dev/null`` like a library is supposed to.
# http://docs.python.org/3.3/howto/logging.html#configuring-logging-for-a-library
logging.getLogger("indigo").addHandler(logging.NullHandler())
