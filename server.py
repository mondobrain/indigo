from typing import List

from fastapi import FastAPI
from pydantic import BaseModel
import spacy

from indigo import KnownEntityRuler

nlp = spacy.load("en_core_web_md")

app = FastAPI()

oper_to_target = {
    "increase": "max",
    "decrease": "min",
    "explain": "max",
    "minimize": "min",
    "maximize": "max",
    "tell me": "max",
    "why": "max",
}


# ---- MODELS ----
class IndigoOptions(BaseModel):
    columns: List[str]
    sentence: str
    interpret = False


# ---- ROUTES ----
@app.get("/")
def read_root():
    return {"status": "ok"}


@app.post("/indigo")
def create_indigo(opts: IndigoOptions):
    pipe = KnownEntityRuler(nlp, opts.columns)

    if nlp.has_pipe(pipe.name):
        nlp.replace_pipe(pipe.name, pipe)
    else:
        nlp.add_pipe(pipe)

    doc = nlp(opts.sentence)

    response = {
        "operation": doc._.operation.text,
        "outcome": doc._.outcome.text,
        "explorable": [col.text for col in doc._.explorable],
    }

    if opts.interpret:
        target = oper_to_target[doc._.operation.text.lower()]
        response["target"] = target

    return response
