# Indigo

Indigo is a Python library for dealing with generating MondoBrain queries from dataframes and plain text.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install mondobrain-indigo
```

## Usage

```python
import indigo
import pandas as pd

df = pd.DataFrame([...])

qds = indigo.load(df)

params = qds("Optimize Engagement for Event Type, Shelter and Duration")
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)